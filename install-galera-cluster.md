# Installing Galera Cluster - 3 Nodes

## Step 0: Machines Preparation
```bash
# update apt lists
sudo apt update
# get nodes ips
sudo apt install net-tools # if ifconfig not installed
ifconfig # to get network IP
```
Edit /etc/hosts
ip machine_name


## Step 1: Install MariaDB
1. Install using ubuntu servers directly 
sudo apt install mariadb-server mariadb-backup
2. set root password
sudo mysql 
password = password("your_password");

## Step 2: Configure /etc/mysql/conf.d/galera.cnf
Required configurations for mysqld
Repeat same stop on other nodes
```bash
[mysqld]
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
bind-address=0.0.0.0

# Galera Provider Configuration
wsrep_on=ON
wsrep_provider=/usr/lib/galera/libgalera_smm.so

# Galera Cluster Configuration
wsrep_cluster_name="test_cluster"
wsrep_cluster_address="gcomm://First_Node_IP,Second_Node_IP,Third_Node_IP"

# Galera Synchronization Configuration
wsrep_sst_method=rsync

# Galera Node Configuration
wsrep_node_address="This_Node_IP"
wsrep_node_name="This_Node_Name"
```
## Step 3: Cluster starting 
```bash
sudo service mysql stop
# stop on all servers

# on the first node
sudo galera_new_cluster
# on other nodes
sudo service mysql start
```
