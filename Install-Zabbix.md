# Installing Zabbix for Monitoring MariaDB Galera Cluster


## Install Zabbix server and front end 
It is recommended to install zabbix server on a monitoring node, outside the cluster.

```bash
# Install server and frontend
wget https://repo.zabbix.com/zabbix/6.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.2-4%2Bubuntu22.04_all.deb
dpkg -i zabbix-release_6.2-4+ubuntu22.04_all.deb
apt update

# Install agent
apt install zabbix-agent

# Control zabbix agent on db servers
systemctl restart zabbix-agent
systemctl enable zabbix-agent
```


```bash
wget https://repo.zabbix.com/zabbix/6.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.2-4%2Bubuntu22.04_all.deb
dpkg -i zabbix-release_6.2-4+ubuntu22.04_all.deb
apt update


apt install zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-sql-scripts zabbix-agent



```

```sql
# mysql -uroot -p
password
mysql> create database zabbix character set utf8mb4 collate utf8mb4_bin;
mysql> create user zabbix@localhost identified by 'password';
mysql> grant all privileges on zabbix.* to zabbix@localhost;
mysql> set global log_bin_trust_function_creators = 1;
mysql> quit
```
```bash
zcat /usr/share/zabbix-sql-scripts/mysql/server.sql.gz | mysql --default-character-set=utf8mb4 -uzabbix -p zabbix
```

```sql
# mysql -uroot -p
password
mysql> set global log_bin_trust_function_creators = 0;
mysql> quit;

```


Edit file /etc/zabbix/zabbix_server.conf

DBPassword=password

```bash
# Control zabbix services
systemctl restart zabbix-server zabbix-agent apache2
systemctl enable zabbix-server zabbix-agent apache2
```