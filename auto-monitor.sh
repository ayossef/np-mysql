#!/bin/bash

export cluster_size=$(sudo mysql -e "show global status like 'wsrep_cluster_size';" | grep wsrep | cut -d$'\t' -f2)
export cluster_uuid=$(sudo mysql -e "show global status like 'wsrep_cluster_state_uuid';" | grep wsrep | cut -d$'\t' -f2)
export local_state=$(sudo mysql -e "show global status like 'wsrep_local_state_comment';" | grep wsrep | cut -d$'\t' -f2)
cd /home/nobleprog/Desktop/np-mysql
echo $(date) >> galera.cluster.log
echo  "Cluster size is" $cluster_size >> galera.cluster.log 
echo "Cluster UUID is" $cluster_uuid >> galera.cluster.log
echo "Node State is " $local_state >> galera.cluster.log
