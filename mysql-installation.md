# MySQL Installation 

1. Update the system (Ubuntu)
```bash
sudo apt update
# get all the new version of the ubuntu programs
```

2. Ask apt to download and install mysql-server
```bash
sudo apt install mysql-server
# download, install and runs mysql server
```

3. Check the status of MySQL server
```bash
sudo service mysql status
```

4. Stop service
```bash
sudo service mysql stop
```

5. start again
```bash
sudo service mysql start
```

6. Finally, connect to MySQL 
```bash
mysql
```