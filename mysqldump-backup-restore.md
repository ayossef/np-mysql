# Mysqldump Backup and Restore


## Backup
### Mysqldump backup all databases for a user

```bash
mysqldump -u admin -p > allserver_dbs.sql
```

### Mysqldump backup a certain database(s)
```bash
mysqldump -u admin -p --databases hrdb > hrdb.sql
```
### Mysqldump backup without data
```bash
mysqldump -u admin -p --databases hrdb --no-data > hrdb_nodata.sql
```


## Restore

### Restore as admin
```bash
mysql -u admin -p < hrdb_backup.sql
```