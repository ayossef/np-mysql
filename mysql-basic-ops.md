# MySQL Administration Tasks

### Create and Admin user using username and password

```sql
create user 'admin'@'localhost' identified by 'adminpass'; 
```

### Grant some permission to this user
- Grant permission (r,m,c,r) on db.*
```sql
grant all privileges on *.* to 'admin'@'localhost';
```

### Apply the privileges
```sql
flush privileges;
```
