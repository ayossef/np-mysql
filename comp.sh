#/bin/bash

diff s02/galera.cluster.log s01/galera.cluster.log > comp.out
cat comp.out | grep size > size.out
cat comp.out | grep UUID > uuid.out
cat comp.out | grep State > state.out


uniq -c size.out | [ $(wc -l) -eq 1 ] && curl http://mysserver/ok?type=size || curl http://mysserver/error?type=diff_size
uniq -c uuid.out | [ $(wc -l) -eq 1 ] && curl http://mysserver/ok?type=uuid || curl http://myserver/error?type=diff_uuid
uniq -c state.out | [ $(wc -l) -eq 1 ] && curl http://mysserver/ok?type=state || curl http://myserver/error?type=diff_state
